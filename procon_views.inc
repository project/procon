<?php

function procon_views_tables() {
  $tables['procon'] = array(
    'name' => 'procon',
    'provider' => 'procon',
    'join' => array(
      'left' => array(
        'table' => 'node',
        'field' => 'nid',
      ),
      'right' => array(
        'field' => 'nid'
      ),
    ),
    "fields" => array(
      "type" => array(
        'name' => t('Procon: Argument type'),
        'help' => t('Display the name of the argument type'),
        'sortable' => TRUE,
      ),
    ),
    "sorts" => array(
      "type" => array(
        'name' => t('Procon: Argument type'),
        'help' => t('Sort the view based on the argument type'),
      ),
    ),
  );

  return $tables;
}

function procon_views_arguments() {
  $arguments = array(
    'argumented_node' => array(
      'name' => t("Procon: Parent Node ID"),
      'handler' => "procon_views_handler_arg_argumented_node",
    ),
  );
  return $arguments;
}

function procon_views_handler_arg_argumented_node($op, & $query, $argtype, $arg = '') {
  switch ($op) {
    case 'summary' :
      $query->ensure_table("procon");
      $query->add_field("parent_nid");
      $fieldinfo['field'] = "procon.parent_nid";
      return $fieldinfo;
    case 'filter' :
      $query->ensure_table("procon");
      $query->add_where("parent_nid = '$arg'");
      $query->add_where("procon.nid = node.nid");
      break;
    case 'link' :
      return l($query->title, "$arg/$query->nid");
    case 'title' :
      if ($query) {
        $term = db_fetch_object(db_query("SELECT title FROM {node} WHERE nid = '%d'", $query));
        return $term->title;
      }
  }
}

function procon_views_default_views() {
  $view = new stdClass();
  $view->name = 'procon_arguments';
  $view->description = t('List arguments of a parent node');
  $view->access = array();
  $view->view_args_php = '';
  $view->page = TRUE;
  $view->page_title = t('Arguments');
  $view->page_header = '';
  $view->page_header_format = '1';
  $view->page_footer = '';
  $view->page_footer_format = '1';
  $view->page_empty = '';
  $view->page_empty_format = '1';
  $view->page_type = 'node';
  $view->url = 'procon_list_arguments';
  $view->use_pager = FALSE;
  $view->nodes_per_page = '99';
  $view->sort = array(
    array(
      'tablename' => 'procon',
      'field' => 'type',
      'sortorder' => 'ASC',
      'options' => '',
    ),
  );
  $view->argument = array(
    array(
      'type' => 'argumented_node',
      'argdefault' => '2',
      'title' => 'Arguments for %1',
      'options' => '',
      'wildcard' => '',
      'wildcard_substitution' => '',
    ),
  );
  $view->field = array();
  $view->filter = array(
    array(
      'tablename' => 'node',
      'field' => 'type',
      'operator' => 'OR',
      'options' => '',
      'value' => array(0 => 'procon_argument'),
    ),
  );
  $view->exposed_filter = array();

  // If Voting API exists also sort the arguments on vote percent.
  if (module_exists('votingapi')) {
    $view->sort[] = array(
      'tablename' => 'votingapi_vote_vote_percent',
      'field' => 'value',
      'sortorder' => 'DESC',
      'options' => '',
    );
    $view->requires = array(procon, votingapi_vote_vote_percent, node);
  }
  else {
    $view->requires = array(procon, node);
  }
  $views[$view->name] = $view;

  return $views;
}

/**
 * Theme the arguments view provided by this module.
 */
function phptemplate_views_view_nodes_procon_arguments($view, $nodes, $type, $teasers = false, $links = true) {
  $first_match = true;

  $output .= '<div id="procon" class="clear-block">'; // Div wrapper to clear floats
  foreach ($nodes as $n) {
    $node = node_load($n->nid);
    $procon_type = $node->procon_type;
    if ($procon_type != $procon_type_prev) {
      if ($first_match) {
        $first_match = false;
      }
      else {
        $output .= '</div>'; // First column closing div
      }
      // Insert div wrapper and heading
      $output .= '<div id="procon-'. $procon_type .'" class="procon-collapsed"><h2 class="procon">'. t('@type arguments', array('@type' => _procon_get_type_name($procon_type))) .'</h2>';
    }
    $node_prepared = node_prepare(node_load($n->nid));
    $output .= node_view($node);
    $output .= comment_render($node);
    $procon_type_prev = $node->procon_type;
  }
  $output .= '</div>'; // Second column closing div
  $output .= '</div>'; // Wrapper closing div

  return $output;
}
