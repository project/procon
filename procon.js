$(document).ready(function(){
  // Comment form
  $("a.procon-add").toggle(function() {
    $(this).parents(".node:first").next().children("div.box").show().find("textarea")[0].focus();
    return false;
  },function() {
    $(this).parents(".node:first").next().children("div.box").hide();
    return false;
  });

  // Collapsible comments
  $("div.comment").addClass('comment-collapsed').click(function() {
    $(this).toggleClass('comment-collapsed');
    $(this).parents(".procon-collapsed").removeClass('procon-collapsed');
  });

  // Show/hide
  function showHide(type) {
    $('<span class="show-all">' + Drupal.settings.procon.show + '</span>').insertBefore("#procon-" + type + " h2.procon").toggle(function() {
      $(this).parent().removeClass('procon-collapsed');
      $("#procon-" + type + " div.comment").removeClass('comment-collapsed');
      $(this).empty().append(Drupal.settings.procon.hide);
    }, function() {
      $("#procon-" + type + " div.comment").addClass('comment-collapsed');
      $(this).empty().append(Drupal.settings.procon.show);
    });
  }
  showHide(1); // Show/hide link for pro
  showHide(2); // For con
});